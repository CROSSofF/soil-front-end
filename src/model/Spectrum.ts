import { Point } from "./Point";

export interface Spectrum {
    points: Point[];
    wavenumber: {
        start: number;
        stop: number;
        step: number;
    }
}