export interface RawSpectrum {
    _id: string;
    id: string;
    metadata: {
        UUID: string;
        ec: number;
        fizzTest: string;
        pH: string;
        sampleType: string;
        totalC: number;
        totalN: number;
    },
    spectrum: number[];
    traceesRef: string;
    wavenumber: {
        start: number;
        stop: number;
        step: number;
    }
}