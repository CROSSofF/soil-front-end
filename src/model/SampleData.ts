export interface SampleData {
    pH?: number;
    ec?: number;
    totalC?: number;
    totalN?: number;
    fizzTest?: "+" | "-" | "NA";
}