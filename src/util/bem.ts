export function bemName(block: string, element?: string, ...modifiers: (string | undefined)[]): string {
    const classNames: string[] = [];
  
    if (element) {
      classNames.push(`${block}__${element}`);
    } else {
      classNames.push(block);
    }
  
    modifiers.forEach((modifier) => {
      if (modifier) {
        classNames.push(`${block}${element ? `__${element}` : ""}_${modifier}`);
      } 
    });
  
    return classNames.join(" "); 
}