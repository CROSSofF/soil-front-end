import { IconProp } from "@fortawesome/fontawesome-svg-core";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import React from "react";
import { bemName } from "../util/bem";

import "./Sidebar.css"

export interface SidebarItem {
    icon: IconProp;
    text: string;
    onClick?: () => void;
}

interface Props {
    items: SidebarItem[];
}

interface State {
    open: boolean;
}

interface ItemProps {
    item: SidebarItem;
    open: boolean;
    className?: string;
}

class Sidebar extends React.Component<Props, State> {
    constructor(props: Props) {
        super(props);

        this.state = {
            open: false,
        }
    }

    public render() {
        const { items } = this.props;
        const { open } = this.state;

        const listJSX = items.map((item, index) => <Sidebar.Item key={`Sidebar-item-${index}`} item={item} open={open} />)

        return (
            <div className={bemName("Sidebar", undefined, open ? "open" : "closed")}>
                <Sidebar.Item item={{icon: "bars", text: "Menu", onClick: this.toggleOpen.bind(this)}} open={open} />
                <div className={bemName("Sidebar", "spacer")} />
                {listJSX}
            </div>
        );
    }

    private toggleOpen() {
        const { open } = this.state;
        this.setState({open: !open});
    }

    private static Item = class extends React.Component<ItemProps> {
        public render() {
            const { open } = this.props;

            return open
                ? this.renderOpen()
                : this.renderClosed();
        }

        private renderOpen(): JSX.Element {
            const { item } = this.props;

            return (
                <div className={bemName("SidebarItem", "container", "open")} onClick={item.onClick ? item.onClick : () => undefined}>
                    <FontAwesomeIcon icon={item.icon} className={bemName("SidebarItem", "icon", "open")}/>
                    <span className={bemName("SidebarItem", "text", "open")}>{item.text}</span>
                </div>
            )
        }

        renderClosed(): JSX.Element {
            const { item } = this.props;
            
            return (
                <div className={bemName("SidebarItem", "container", "closed")} onClick={item.onClick ? item.onClick : () => undefined}>
                    <FontAwesomeIcon icon={item.icon} className={bemName("SidebarItem", "icon", "closed")}/>
                    <span className={bemName("SidebarItem", "text", "closed")}>{item.text}</span>
                </div>
            )
        }
    }
}

export default Sidebar;