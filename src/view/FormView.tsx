import React, {ChangeEvent} from 'react';
import './App.css';

import { Dropdown, Form, Loader, Input, InputOnChangeData, DropdownProps,Label } from 'semantic-ui-react';
import { v4 } from 'uuid';
import { toast } from 'react-toastify';
import { SampleData } from '../model/SampleData';

import "./FormView.css"

interface Props {

}

interface FormData {
    pH: string;
    ecValue: string;
    ecUnit: string;
    totalC: string;
    totalN: string;
    fizzTest: string;
}

interface State {
    formData: FormData;
    sendingFormData: boolean;
}

class FormView extends React.Component<Props, State> {
    private onChangePH: (event: ChangeEvent<HTMLInputElement>, data: InputOnChangeData) => void;
    private onChangeECUnit: (event: React.SyntheticEvent<HTMLElement, Event>, data: DropdownProps) => void;
    private onChangeTotalC: (event: ChangeEvent<HTMLInputElement>, data: InputOnChangeData) => void;
    private onChangeTotalN: (event: ChangeEvent<HTMLInputElement>, data: InputOnChangeData) => void;
    private onChangeFizzTest: (event: React.SyntheticEvent<HTMLElement, Event>, data: DropdownProps) => void;   

    private static initialFormDataState = {
        pH: "",
        ecValue: "",
        ecUnit: "0.001",
        totalC: "",
        totalN: "",
        fizzTest: ""
    };
    
    constructor(props: Props) {
        super(props);

        this.onChangePH = this.onInputChange.bind(this, "pH", (entry) => this.isInRange(entry, 0, 14));
        this.onChangeECUnit = this.onDropdownChange.bind(this, "ecUnit");
        this.onChangeTotalC = this.onInputChange.bind(this, "totalC", (entry) => this.isInRange(entry, 0, 100));
        this.onChangeTotalN = this.onInputChange.bind(this, "totalN", (entry) => this.isInRange(entry, 0, 100));
        this.onChangeFizzTest = this.onDropdownChange.bind(this, "fizzTest");

        this.onECValueChange = this.onECValueChange.bind(this);
        this.onSubmit = this.onSubmit.bind(this);

        this.state ={
            formData: FormView.initialFormDataState,
            sendingFormData: false
        }
    }

    private onInputChange(
        field: keyof FormData,
        validator: (entry: string) => boolean,
        event: ChangeEvent<HTMLInputElement>,
        data: InputOnChangeData,
    ){
        let newState = {...this.state};
        const { value } = data;
        
        if (validator(value)) {
            newState.formData[field] = value as any;
        }
                
        this.setState(newState);
    }

    private onDropdownChange(
        field: keyof FormData,
        event: React.SyntheticEvent<HTMLElement, Event>,
        data: DropdownProps
    ){
        let newState = {...this.state};
        newState.formData[field] = data.value as string;

        this.setState(newState);
    }

    private onECValueChange(
        event: ChangeEvent<HTMLInputElement>,
        data: InputOnChangeData,
    ){
        let newState = {...this.state};
        
        if (this.isInRange(data.value, 0, 10000)) {
            if (parseFloat(data.value) >= 1000 && this.state.formData.ecUnit === "0.001") {
                const newECValue = parseFloat(data.value) * parseFloat(this.state.formData.ecUnit);
                newState.formData.ecValue = newECValue.toString();
                newState.formData.ecUnit = "1";
            }
            else if (data.value === "") {
                newState.formData.ecUnit = "0.001";
                newState.formData.ecValue = data.value;
            }
            else {
                newState.formData.ecValue = data.value;
            }
        }
        this.setState(newState);
    }

    private chekcIfFloatOrEmpty = /^\d*\.?\d{0,4}$/;

    private isInRange(entry: string, min: number, max: number) {
        const isFloat = this.chekcIfFloatOrEmpty.test(entry);
        
        if (isFloat) {
            const value = parseFloat(entry);
            return entry === "" || (value <= max && value >= min);
        }

        return false;
    }

    private onSubmit() {
        const { pH, ecValue, ecUnit, totalC, totalN, fizzTest } = this.state.formData;

        let newState = {...this.state};

        const headers: HeadersInit = new Headers();
        headers.set("Content-Type", "application/json");

        const sampleData: SampleData = {};
        if (pH !== "") sampleData.pH = parseFloat(pH);
        if (ecValue !== "" && ecUnit !== "") sampleData.ec = parseFloat(ecValue) * parseFloat(ecUnit) * 0.001;
        if (totalC !== "") sampleData.totalC = parseFloat(totalC);
        if (totalN !== "") sampleData.totalN = parseFloat(totalN);
        if (fizzTest !== "") sampleData.fizzTest = fizzTest as any;

        const body = JSON.stringify(sampleData) as any;
        
        newState.sendingFormData = true;
        this.setState(newState, () => {
            fetch(`/data/${v4()}`,{
                method: "POST",
                headers,
                body,
            })
            .then((response) => {
                switch (response.status) {
                    case 200:
                        console.log(response);
                        toast("Data was uploaded successfully", {
                            type: "success",
                            style: { background: "#21ba45" },
                        });
                        break;
                    case 500:
                        toast("500: Whoops", {
                            type: "error",
                            style: {background: "#db2828"}
                        });
                        break;
                    default:
                        toast("Unknown Error, data was not uploaded", {
                            type: "error",
                            style: {background: "#db2828"}
                        });
                        console.error(`Error Code: ${response.status}, `, response)
                }
                
                let resetState = {...this.state};
                resetState.sendingFormData = false;
                resetState.formData = FormView.initialFormDataState;
                this.setState(resetState);
            })
            .catch((reason) => {
                console.error(`fetch failed: ${reason}`);
                toast("Data could not be uploaded", {
                    type: "error",
                    style: {background: "#db2828"}
                });

                let resetState = {...this.state};
                resetState.sendingFormData = false;
                resetState.formData = FormView.initialFormDataState;
                this.setState(resetState);
            });
        });       
    }

    private ecOptions = [
        {key: 'mS', text: 'mS', value: '1'},
        {key: 'uS', text: 'μS', value: '0.001'}
    ];

    private fizzTestOptions = [
        {key: '+', text: 'Positive', value: '+'},
        {key: '-', text: 'Negative', value: '-'}
    ];

    public render() {
        const { formData, sendingFormData } = this.state
    
        return (
            <Form className="Form" inverted>
                <Form.Field className="Form__field">
                    <Label
                        className="Form__fieldLabel big"
                        content="pH"
                    />
                    <Input
                        className="Form__input"
                        label={{content: "pH"}}
                        labelPosition="right"
                        placeholder="0 < pH < 14"
                        step="0.1"
                        value={formData.pH}
                        onChange={this.onChangePH}
                        
                    />
                </Form.Field>
                <Form.Field>
                    <Label
                        className="Form__fieldLabel big"
                        content="Electrical Conductivity"   
                    />
                    <Input
                        className="Form_dropdownInput"
                        label={<Dropdown
                            value={formData.ecUnit}
                            options = {this.ecOptions} 
                            onChange={this.onChangeECUnit}    
                        />}
                        labelPosition="right" 
                        placeholder="EC     e.g. 0.38 mS"
                        value={formData.ecValue}
                        onChange={this.onECValueChange}
                    />
                </Form.Field>
                <Form.Field>
                    <Label
                        className="Form__fieldLabel big"
                        content="Total Carbon"   
                    />
                    <Input 
                        label={"%"}
                        labelPosition="right"
                        value={formData.totalC}
                        placeholder="e.g. 0.38"
                        onChange={this.onChangeTotalC}
                    />
                </Form.Field>
                <Form.Field>
                    <Label
                        className="Form__fieldLabel big"
                        content="Total Nitrogen"   
                    />
                    <Input
                        label={"%"}
                        labelPosition="right"
                        value={formData.totalN}                        
                        placeholder="e.g. 0.38"
                        onChange={this.onChangeTotalN}
                    />
                </Form.Field>
                <Form.Dropdown 
                    clearable 
                    className={"Form__dropdown"}
                    options={this.fizzTestOptions}
                    value={formData.fizzTest}
                    label={<Label className="Form__fieldLabel big" content="Fizz Test" />}
                    onChange={this.onChangeFizzTest}
                    selection
                />
                <div style={{height: "30pt"}} />
                <Form.Button
                    content={sendingFormData ? <Loader /> : "Submit"}
                    onClick={this.onSubmit}
                    disabled={sendingFormData}

                />         
            </Form>
        );
    }
}

export default FormView;
