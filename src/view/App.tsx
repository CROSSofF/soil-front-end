import { push } from 'connected-react-router';
import React from 'react';
import { connect } from 'react-redux';
import { bemName } from '../util/bem';
import './App.css';
import Sidebar from './Sidebar';

interface OwnProps {
    children?: any;
}
interface DispatchProps {
    pushHome: () => any;
    pushGraph: () => any;
    pushWavenumberTable: () => any;
    pushForm: () => any;
}
type Props = OwnProps & DispatchProps;

class App extends React.Component<Props> {
    public render() {
        const { pushHome, pushGraph, pushWavenumberTable, pushForm } = this.props;

        const items = [
            {icon: "home", text: "Home", onClick: pushHome.bind(this)},
            {icon: "chart-line", text: "LineChart", onClick: pushGraph.bind(this)},
            {icon: "table", text: "WavenumberTable", onClick: pushWavenumberTable.bind(this)},
            {icon: "file-upload", text: "Form", onClick: pushForm.bind(this)},
        ];

        return (
            <div className={bemName("App")}>
                <Sidebar items={items as any}/>
                <div className={bemName("App", "body")}>
                    {this.props.children}
                </div>
            </div>
        );
    }
}

const mapDispatchToProps = (dispatch: any) => ({
    pushHome: () => dispatch(push("/home")),
    pushGraph: () => dispatch(push("/graph")),
    pushWavenumberTable: () => dispatch(push("/wavenumbers")),
    pushForm: () => dispatch(push("/form")),
})

export default connect(
    null, 
    mapDispatchToProps
)(App);
