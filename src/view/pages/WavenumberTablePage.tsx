import React from "react";

interface OwnProps {

}
type Props = OwnProps;

interface State {
    loading: boolean;
}

class WavenumberTablePage extends React.Component<Props, State> {
    constructor(props: Props) {
        super(props);

        this.state = {
            loading: true,
        }
    }

    public componentDidMount() {
        fetch("/wavenumbers")
        .then((response) => {
            response.json()
            .then((jsonValue) => {
                
            })
        })
    }

    public render() {
        return null;
    }
}

export default WavenumberTablePage;