import React from "react";
import { Point } from "../../model/Point";
import { RawSpectrum } from "../../model/RawSpectrum";
import { Spectrum } from "../../model/Spectrum";
import { bemName } from "../../util/bem";

import ReactInputPosition from "react-input-position";

import "./GraphPage.css"
import SpectrumGraph from "../components/SVGGraph";

interface Props {

}

interface State {
    loading: boolean;
    data?: Spectrum;

    zoomLevel: number;
    center: Point;
}

class GraphPage extends React.Component<Props, State> {
    constructor(props: Props) {
        super(props);

        this.state = {
            loading: true,
            zoomLevel: 1.,
            center: { x: 1920/2, y: 1080/ 2}
        }
    }

    public componentDidMount() {
        fetch("/data")
        .then((response) => response.json())
        .then((jsonData) => {
            const data = jsonData[0] as RawSpectrum;
            const { spectrum } = data;
            const { start, stop, step } = data.wavenumber;
        
            const points: Point[] = [];

            var i = 0;
            for (var x = start; x >= stop; x -= step) {
                points.push({x, y: spectrum[i]});
                i++;
            }

            this.setState({ loading: false, data: { points, wavenumber: data.wavenumber } });
            console.log(jsonData[0]);
        }); 
    }

    public render() {
        const { data } = this.state;

        if (data) {                            
            return (
                    <ReactInputPosition
                        trackItemPosition
                        itemPositionLimitBySize
                        centerItemOnActivatePos
                        trackPassivePosition
                        className={bemName("GraphPage")}
                    >
                        <SpectrumGraph spectrum={data} />
                    </ReactInputPosition>
            );
        } else {
            return null;
        }
    }
}

export default GraphPage;