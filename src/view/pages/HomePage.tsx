import React from "react";
import { Button } from "semantic-ui-react";
import { v4 } from "uuid";


interface State {
    loading: boolean;
    data: object | null;
}

class HomePage extends React.Component<{}, State> {
    constructor(props: {}) {
        super(props);

        this.state = {
            loading: true,
            data: null
        }

        fetch("/data")
        .then((response) => {
            response.json()
            .then((value) => {
                this.setState({loading: false, data: value});
            })
        })
    }

    public render() {
        const tenantID = "1c9fb917-6d1b-4fc4-9869-4631c2909991";
        const clientID = "795be373-48c0-4eb4-ab0e-168876d804d6";
        const responseType = "id_token";
        const scope = encodeURIComponent("openid profile");
        const responseMode = "fragment";
        const redirectURI = encodeURIComponent("https://localhost:8443/auth/redirect/");
        const nonce = v4();
        const state = v4();

        return (
            <Button 
                content="Login with microsoft" 
                onClick={() => window.location.href = `https://login.microsoft.com/${tenantID}/oauth2/v2.0/authorize?client_id=${clientID}&response_type=${responseType}&scope=${scope}&response_mode=${responseMode}&redirect_uri=${redirectURI}&nonce=${nonce}&state=${state}`} 
            />
        );
    }
}

export default HomePage;