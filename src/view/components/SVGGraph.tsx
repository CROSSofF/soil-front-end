import React from "react";
import { v4 } from "uuid";
import { Point } from "../../model/Point";
import { Spectrum } from "../../model/Spectrum";
import { bemName } from "../../util/bem";

import "./SVGGraph.css"

interface GraphProps {
    spectrum: Spectrum; 
    passivePosition?: Point;
}

interface GraphState {
    pointString: string;
    startX: number;
    stopX: number;
    zoom: {
        scrollTotal: number;
    },
    pan: {
        lastClientX: number | null;
    }
    shiftDown: boolean;
}

class SpectrumGraph extends React.Component<GraphProps, GraphState> {
    private lowerBound = 0.7;
    private upperBound = 1.4;

    private maxZoom = 1;
    private minZoom = 0.01;

    private majorTick = 1000;

    private containerRef: React.RefObject<HTMLDivElement>;

    constructor(props: GraphProps) {
        super(props);

        const { spectrum: { points } } = props;

        this.recalculateGraph = this.recalculateGraph.bind(this);
        this.containerRef = React.createRef();

        this.state = {
            pointString: "",
            zoom: {
                scrollTotal: 0,
            },
            pan: {
                lastClientX: null,
            },
            startX: points[0].x,
            stopX: points[points.length - 1].x,
            shiftDown: false,
        }
    }
    
    private recalculateGraph() {
        const currentContainer = this.containerRef.current;

        if (currentContainer) {
            this.setState({
                pointString: this.generatePointsString(
                    currentContainer.clientWidth, 
                    currentContainer.clientHeight
                )
            });
        } else {
            throw new Error("Container not mounted. Can't calculate graph.");
        }
    }

    private zoomCurve = (x: number) => this.clamp(0.99*Math.exp(0.0015*x) + 0.01, this.minZoom, this.maxZoom);

    private zoom(event: WheelEvent) {
        const { clientX, deltaY: scrollAmount } = event;
        const { zoom: { scrollTotal: oldScrollTotal }, startX: oldStartX, stopX: oldStopX } = this.state;
        const { spectrum: { points } } = this.props;
        const { containerRef: { current: currentContainer } } = this;
        
        if (!currentContainer) {
            throw new Error("Container ref not mounted. Can't calculate zoom level.")
        }
        
        const { clientWidth: width } = currentContainer;
        const leftOffset = currentContainer.offsetParent 
                ? (currentContainer.offsetParent as any).offsetLeft 
                    ? (currentContainer.offsetParent as any).offsetLeft 
                    : 0
                : 0;

        const newScrollTotal = this.clamp(oldScrollTotal + scrollAmount, -3000, 0);

        const datasetScale = this.zoomCurve(newScrollTotal)/this.zoomCurve(oldScrollTotal);
        const relativeCursorPos = (clientX - leftOffset)/width;
        const oldDomain = oldStartX - oldStopX;

        const dLeftX = relativeCursorPos * (oldDomain * datasetScale);
        const dRightX = (1 - relativeCursorPos) * (oldDomain * datasetScale);

        const absoluteCursorPos = oldStopX + ((1 - relativeCursorPos) * oldDomain);

        const newStartX = newScrollTotal === 0 // FIXME: THIS IS BAD 
                ? points[0].x                  // ACTUALLY FIX THE FLOATING POINT ACCUMULATION ERROR AT SOME POINT PLS
                : absoluteCursorPos + dLeftX;
        const newStopX = newScrollTotal === 0 
                ? points[points.length - 1].x 
                : absoluteCursorPos - dRightX;

        this.setState({
            zoom: {
                scrollTotal: newScrollTotal,
            },
            startX: newStartX,
            stopX: newStopX,
        }, this.recalculateGraph)
    }

    private keydownHandler(event: KeyboardEvent) {
        if (event.shiftKey) {
            this.setState({shiftDown: true})
        }
    }

    private keyupHandler(event: KeyboardEvent) {
        if (!event.shiftKey) {
            this.setState({shiftDown: false})
        }
    }

    private mousedownHandler(event: MouseEvent) {
        if (!event.shiftKey) {
            this.setState({ pan: { lastClientX: event.clientX } })
        }
    }

    private mouseupHandler(event: MouseEvent) {
        this.setState({ pan: { lastClientX: null } })
    }

    private mousemoveHandler(event: MouseEvent) {
        if (!event.shiftKey && (event.buttons & 1)) {
            const { containerRef: { current: currentContainer } } = this;
            const { pan: { lastClientX }, startX, stopX } = this.state;
            const { spectrum: { points } } = this.props;

            if (currentContainer && lastClientX) {
                const dx = (startX - stopX)* ((lastClientX - event.clientX)/currentContainer.clientWidth);

                let newStartX = startX - dx;
                let newStopX = stopX - dx;
                
                if (newStartX > points[0].x) {
                    const dx2 = newStartX - points[0].x;
                    newStartX -= dx2;
                    newStopX -= dx2;
                } else if (newStopX < points[points.length - 1].x) {
                    const dx2 = points[points.length - 1].x - newStopX;
                    newStartX += dx2;
                    newStopX += dx2;
                }

                this.setState({ pan: {lastClientX: event.clientX }, startX: newStartX, stopX: newStopX }, this.recalculateGraph);
            } else {
                this.setState({ pan: { lastClientX: event.clientX } });
            }
        }
    }

    public componentDidMount() {
        const containerCurrent = this.containerRef.current

        if (containerCurrent) {
            window.addEventListener("resize", this.recalculateGraph);
            containerCurrent.addEventListener("wheel", this.zoom.bind(this));
            window.addEventListener("keydown", this.keydownHandler.bind(this));
            window.addEventListener("keyup", this.keyupHandler.bind(this));
            containerCurrent.addEventListener("mousedown", this.mousedownHandler.bind(this));
            containerCurrent.addEventListener("mouseup", this.mouseupHandler.bind(this));
            containerCurrent.addEventListener("mousemove", this.mousemoveHandler.bind(this));

            this.recalculateGraph();
        } else {
            throw new Error("Container ref not set. Can't bind listeners")
        }
    }

    public componentWillUnmount() {
        const containerCurrent = this.containerRef.current

        if (containerCurrent) {
            window.removeEventListener("resize", this.recalculateGraph);
            containerCurrent.removeEventListener("wheel", this.zoom);
            window.removeEventListener("keydown", this.keydownHandler);
            window.removeEventListener("keyup", this.keyupHandler);
            containerCurrent.removeEventListener("mousedown", this.mousedownHandler);
            containerCurrent.removeEventListener("mouseup", this.mouseupHandler);
            containerCurrent.removeEventListener("mousemove", this.mousemoveHandler);
        } else {
            throw new Error("Container ref not set. Can't unbind listeners")
        }
        
    }

    private lerp(start: number, stop: number, value: number): number {
        return 1 - (value-start)/(stop-start);
    }

    private activePointsFilter = (startX: number, stopX: number, point: Point) => point.x <= startX && point.x >= stopX;

    private generatePointsString(pageWidth: number, pageHeight: number): string {
        const { points: allPoints } = this.props.spectrum;
        const { startX, stopX } = this.state
        const points = allPoints.filter(this.activePointsFilter.bind(this, startX, stopX));

        const results: string[] = [];

        for (let i = 0; i < pageWidth; i++) {
            results.push(`${i},${this.getYPixel(i, points, pageWidth, pageHeight)}`);
        }

        return results.join(" ");
    }

    private clamp(x: number, min: number, max: number) {
        return Math.min(Math.max(x, min), max);
    }

    private getYPixel(xPos: number, points: Point[], width: number, height: number) {
        const index_x1 = this.calculatePointsIndex(xPos, width, points);        

        const x1 = points[index_x1].x;
        const x2 = points[index_x1+1].x;

        const y1 = points[index_x1].y;
        const y2 = points[index_x1+1].y;

        const dx = (x2 - x1) * this.calcPointsIndexRemainder(xPos, width, points);
        
        const m = (y2 - y1)/(x2 - x1);

        return this.lerp(this.lowerBound, this.upperBound, y1 + m*dx)*height;
    }

    private calculatePointsIndex(xPos: number, width: number, points: Point[]) {
        const approxIndex = xPos * points.length/width;
        return this.clamp(Math.floor(approxIndex), 0, points.length-2);
    }

    private calcPointsIndexRemainder(xPos: number, width: number, points: Point[]) {
        return ( xPos * points.length/width) % 1;
    }

    public render() {
        const { shiftDown } = this.state;
        const currentContainer = this.containerRef.current;
 
        if (!currentContainer) {
            return <div ref={this.containerRef} className={bemName("SVGGraph")}/>;
        }

        const { clientWidth: width, clientHeight: height} = currentContainer;

        return (
            <div ref={this.containerRef} className={bemName("SVGGraph")}>
                <svg className={bemName("SVGGraph", "graph", shiftDown ? "pan" : undefined)} viewBox={`0 0 ${width} ${height}`}>
                    {this.renderPlot()}
                    {this.renderXAxis()}
                    {this.renderCursorLine()}
                </svg>
            </div>  
        );
    }

    private renderPlot() {
        const { pointString } = this.state;

        return (
            <polyline 
                className={bemName("SVGGraph", "plot")}
                points={pointString}    
            />
        );
    }

    private renderCursorLine() {
        const { passivePosition, spectrum: { points: allPoints } } = this.props;
        const { containerRef: { current: currentContainer } } = this;
        const { startX, stopX } = this.state

        if (!passivePosition || !currentContainer) {
            return null;
        } 

        const points = allPoints.filter(this.activePointsFilter.bind(this, startX, stopX))

        const { x: lineX } = passivePosition;
        const { clientWidth: width, clientHeight: height } = currentContainer;

        const pointsIndex = this.calculatePointsIndex(lineX, width, points);
        const textYPos = this.getYPixel(lineX, points, width, height);

        const lineJSX = (
            <line
                className={bemName("SVGGraph", "cursor")}
                key={`SVGPlot-cursorLine-${v4()}`}
                x1={lineX}
                x2={lineX}
                y1={height - 25}
                y2={0}
            />
        );

        const textBoxJSX = (
            <g key={`SVGPlot-textGroup-${v4()}`} transform={`translate(${lineX+1}, ${textYPos})`}>
                <rect 
                    className={bemName("SVGGraph", "textBox")}
                    x={lineX < width - 100 ? 0 : -102} 
                    y={-120} 
                    width={100} 
                    height="44" 
                />
                <text 
                    className={bemName("SVGGraph", "text")}
                    x={lineX < width - 100 ? 10 : -90}
                    y={-100}
                >
                    λ⁻¹: {points[pointsIndex].x} cm⁻¹
                </text>
                <text
                    className={bemName("SVGGraph", "text")}
                    x={lineX < width - 100 ? 10 : -90}
                    y={-100+16} 
                >
                    A: {points[pointsIndex].y}
                </text>  
            </g>
        );

        return [ lineJSX, textBoxJSX ];
    }

    private renderXAxis() {
        const { containerRef: { current: currentContainer }, majorTick } = this;
        const { startX, stopX } = this.state;


        if (!currentContainer) {
            return null;
        }

        const { clientWidth: width, clientHeight: height } = currentContainer;
        const tickHeight = 8;
        
        const textJSX = [
            (
                <text 
                    className={bemName("SVGGraph", "text", "small")}
                    key={`SVGGraph-xAxisText-${v4()}`} 
                    x={1} 
                    y={20} 
                    textAnchor="start"
                >
                    {Math.round(startX)}
                </text>
            ),
            (
                <text 
                    className={bemName("SVGGraph", "text", "small")}
                    key={`SVGGraph-xAxisText-${v4()}`} 
                    x={width-1} 
                    y={20} 
                    textAnchor="end "
                >
                    {Math.round(stopX)}
                </text>
            )
        ];

        // Calculate wavenumbers per pixel
        const lambdaPerPixel = (startX - stopX)/width;
        
        let pathString = `M 1,${tickHeight} `
                       + `l 0,${-tickHeight} `;  

        // Generate ticks
        let lambdaPos = startX;
        let dx = (lambdaPos % majorTick)/lambdaPerPixel - 1;

        pathString += this.tickString(dx, tickHeight);
        lambdaPos -= lambdaPos % majorTick;

        for (lambdaPos; lambdaPos > stopX + majorTick; lambdaPos -= majorTick) {
            pathString += this.tickString(majorTick/lambdaPerPixel, tickHeight);
            
            textJSX.push(
                <text
                    className={bemName("SVGGraph", "text", "small")}
                    key={`SVGGraph-xAxisText-${v4()}`} 
                    x={dx}
                    y={20}
                    textAnchor={"middle"}
                >
                    {Math.round(lambdaPos - (lambdaPos % majorTick))} 
                </text>
            );
            dx += majorTick/lambdaPerPixel;
        }

        textJSX.push(
            <text
                className={bemName("SVGGraph", "text", "small")}
                key={`SVGGraph-xAxisText-${v4()}`} 
                x={dx + 0.5}
                y={20}
                textAnchor={"middle"}
            >
                {Math.round(lambdaPos - (lambdaPos % majorTick))} 
            </text>
        );

        pathString += `L ${width - 1},${0}`
                    + `l 0,${tickHeight}`;


        return (
            <g transform={`translate(0, ${height - 25})`}>
                <path
                    className={bemName("SVGGraph", "xAxis")}
                    d={pathString}
                />
                {textJSX}
            </g>
        );
    }

    private tickString(dx: number, tickHeight: number) {
        //      move dx   tick down         tick up
        return `l ${dx},0 l 0,${tickHeight} l 0,${-tickHeight} `;
    }
}

export default SpectrumGraph;