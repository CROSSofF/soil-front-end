import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './view/App';
import reportWebVitals from './reportWebVitals';

import { createBrowserHistory } from "history";
import { Provider } from "react-redux";
import { applyMiddleware, combineReducers, compose, createStore } from "redux";
import { Route, Switch } from "react-router";

import fontawesome from "@fortawesome/fontawesome";
import { faBars, faChartLine, faFileUpload, faHome, faTable } from '@fortawesome/free-solid-svg-icons';
import { ConnectedRouter, connectRouter, routerMiddleware } from 'connected-react-router';
import HomePage from './view/pages/HomePage';
import GraphPage from './view/pages/GraphPage';
import FormPage from './view/pages/FormPage';

import "semantic-ui-css/semantic.min.css";
import "react-toastify/dist/ReactToastify.min.css";
import { ToastContainer } from 'react-toastify';

fontawesome.library.add(
    faBars as any,
    faHome as any,
    faChartLine as any,
    faTable as any,
    faFileUpload as any,
);

const history = createBrowserHistory();
const createRootReducer = (history: any) => combineReducers({
    router: connectRouter(history),
  })

const store = createStore(
    createRootReducer(history),
    {},
    compose(
        applyMiddleware(
            routerMiddleware(history),
        ),
    ),
)

ReactDOM.render(
    <React.StrictMode>
        <Provider store={store}>
            <App>
                <ConnectedRouter history={history}>  
                    <Switch>
                        <Route path={"/home"} component={HomePage} />   
                        <Route path={"/graph"} component={GraphPage} />
                        <Route path={"/form"} component={FormPage} />
                    </Switch>    
                </ConnectedRouter>
            </App>
        </Provider>
        <ToastContainer /*style={{zIndex: 1001, position: 'absolute', width: "100vw", height: "100vh"}}*//>
    </React.StrictMode>,
    document.getElementById('root')
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
